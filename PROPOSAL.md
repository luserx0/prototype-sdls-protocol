Space Data Link Security Protocol
---
## Mentor: Artur Scholz (artur.scholz@librecube.org)
## Harry Pantazis (luserx0@gmail.com)
*_April 30, 2019***_

### Description: 
The purpose of the Security Protocol is to provide a secure standard method, with associated data structures, for performing security functions on octet-aligned user data within Space Data Link Protocol Transfer Frames over a space link. The maximum length of input data that can be accommodated is not limited by the Security Protocol, but is an attribute of the related Space Data Link Protocol. 
Both Security Header and Trailer are provided for delimiting the protected data and conveying the necessary cryptographic parameters within Transfer Frames. The size of the Security Header and Trailer reduces the maximum size of the Transfer Frame Data Field allowed by the underlying Space Data Link Protocol.
The Security Protocol preserves the quality of service that is provided by the Space Data Link Protocol. The Security Protocol is scalable to operate across any number of Virtual Channels supported by the Space Data Link Protocols. 
The Security Protocol provides authentication for some fields in the Transfer Frame Primary Header and for some auxiliary data fields in a TM Transfer Frame. It does not provide encryption for these fields. The Security Protocol can provide authentication protection for the service data of the Virtual Channel Frame Secondary Header (VC_FSH) Service and simplex (one-way), stateful cryptographic session for providing authentication, data integrity, replay protection, and/or data confidentiality for defining the cryptographiccommunications parameters to be used by both the sending and receiving ends of a communications session, and for maintaining state information for the duration of the session.


### Plan of Action:
Undeniably a very big obstacle is the standard itself, due to the open ends it leaves and the numerous references it contains. The idea it to prototype the implementation for CCSDS 355 (alias: SDLS Protocol) in Python 3 with the help of the PyCrypto library. Th stretch goal would be to get invlolved with the precompiler and port some core pieces to Cython to speed up processes; but as a concept it is sane to use Python for start to get to a setisfying implementation of the protocol, secure and tested.
The starting point would be to “crunch” the standards before the coding begins and figure out which parts of the standard are useful and to what degree.

My proposal is to use: 
  - AES with 256-bit keys and GCM as Encryption Algorithms (even though the AEAD mode specified in the standard is interesting as a stretch goal).
  - Digital Signature based Authentication implementing the ECDSA cryptographic algorithm to incorporate elliptic curve cryptography with the use of curve25519 (RFC), while Poly1305 is also an interesting thought. Overall goal is the incorporation of any post-quantum cryptographic measure.
  - Authenticated encryption (++)
  - Implementation of the services support described in Table 5.1, Section 5.4 in CCSDS 355.

  The prototype will use ZMQ to demonstrate it’s functionalities between nodes, wither between laptops or through OpenWRT devices or even Single-board computers. 

### Risks - Potential Problems:
Incompatibility with other existing FOSS implementations that use the SDLS Protocol.
Minimum Level of Success:
Working prototype that adheres to the standards able to send and receive secure frames.

### Milestones:
- Cryptographic Algorithms in Encryption, Authentication and Encrypted Authentication decision
- Implementation of Data Units (Security Header and Security trailer) and associated data structures to support Procedures
- Draft of Security Association Management Procedures (minimum functionality)
- ApplySecurity function and Sending Procedures
- ProcessSecurity function and Receiving Procedures
- Further development of SAMP
- Documentation on how stuff works and the approach of the implementation.


